﻿using System;
using System.Collections.Generic;
using System.IO;
using TMPro;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using UnityEngine;
using Random = UnityEngine.Random;

public class BoxAgent : Agent
{
    [SerializeField] private Transform target;
    Rigidbody myAgent;
    public EnviromentArea EnviromentArea;
    public GameObject[] spawnField;
    public TextMeshProUGUI rewardText;
    public GameObject tail;

    private float timeElapsed;
    private float rewardInterval = 1f;
    private HashSet<Vector3> visitedStates = new HashSet<Vector3>();
    private float scaleTrainDistance;
    private int reachGoalCount = 0;
    private int[] scaleDistance = new int[] { 8, 4, 2 };

    // brain ашиглавал үүнийг true болгох
    public bool finalScale;

    // yvsan gazraa track hiih bol trur bolgoh
    public bool isNeedTail;

    private float epsilon = 0.8f;
    private float minEpsilon = 0.05f;
    private float epsilonDecay = 0.99981f;

    //For Json
    private int episode = 0;
    private bool takeReward;
    private List<Information> AllInformations = new List<Information>();

    public override void Initialize()
    {
        myAgent = GetComponent<Rigidbody>();
    }

    private void AgentMove(ActionSegment<int> actionsDiscreteActions)
    {
        var dirToGo = Vector3.zero;
        var rotateDir = Vector3.zero;

        if (Random.value < epsilon)
        {
            actionsDiscreteActions[0] = generateRandomValue();
        }
        else
        {
            var action = actionsDiscreteActions[0];
            switch (action)
            {
                case 1:
                    dirToGo = transform.forward * 1f;
                    break;
                case 2:
                    rotateDir = transform.up * 1f;
                    break;
                case 3:
                    rotateDir = transform.up * -1f;
                    break;
                case 4:
                    dirToGo = transform.forward * -1f;
                    break;
            }

            transform.Rotate(rotateDir, Time.deltaTime * 200f);
            myAgent.AddForce(dirToGo * 1f, ForceMode.VelocityChange);
        }
    }

    // ReSharper disable Unity.PerformanceAnalysis
    public override void OnActionReceived(ActionBuffers actions)
    {
        if (isNeedTail)
        {
            CreateTail();
        }

        AgentMove(actions.DiscreteActions);

        epsilon = Mathf.Max(epsilon * epsilonDecay, minEpsilon);

        Vector3 formatPosition = RoundVector3(transform.position);
        if (!visitedStates.Contains(formatPosition))
        {
            Debug.Log("new visitedStates");
            AddReward(0.001f);
            visitedStates.Add(formatPosition);
        }
        else
        {
            Debug.Log("old visitedStates");
            AddReward(-1f / MaxStep);
        }

        if (Vector3.Distance(transform.localPosition, target.localPosition) < scaleTrainDistance && !finalScale)
        {
            Debug.Log("Collided with goal. but have scale");
            AddReward(2f);
            reachGoalCount++;
            takeReward = true;
            JsonWrite();
            EndEpisode();
        }

        if (StepCount >= MaxStep)
        {
            EndEpisodeCustom();
        }
    }


    public override void CollectObservations(VectorSensor sensor)
    {
        var agentPosition = transform.localPosition;
        var targetPosition = target.localPosition;
        sensor.AddObservation(agentPosition);
        sensor.AddObservation(targetPosition);
        sensor.AddObservation(transform.forward);
        sensor.AddObservation(Vector3.Distance(agentPosition, targetPosition));
        sensor.AddObservation(transform.InverseTransformDirection(myAgent.velocity));
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("goal"))
        {
            Debug.Log("Collided with goal.");
            AddReward(5f);
            reachGoalCount++;
            takeReward = true;
            JsonWrite();
            EndEpisode();
        }
        else if (collision.gameObject.CompareTag("wall") || collision.gameObject.CompareTag("box"))
        {
            Debug.Log("Collided with wall.");
            AddReward(-0.5f);
        }
        else if (collision.gameObject.CompareTag("dynamicObstacle"))
        {
            Debug.Log("Collided with dynamicObstacle.");
            AddReward(-1f);
        }
    }


    public override void OnEpisodeBegin()
    {
        takeReward = false;
        episode++;
        if (reachGoalCount < 100)
        {
            Debug.Log("Scale first");
            scaleTrainDistance = scaleDistance[0];
        }
        else if (reachGoalCount < 200)
        {
            Debug.Log("Scale second");
            scaleTrainDistance = scaleDistance[1];
        }
        else if (reachGoalCount < 300)
        {
            Debug.Log("Scale third");
            scaleTrainDistance = scaleDistance[2];
        }
        else
        {
            Debug.Log("Scale final");
            scaleTrainDistance = 0;
            finalScale = true;
        }

        EnviromentArea.ClearEnviroment();
        SpawnAgent();
        EnviromentArea.placeBall();
        EnviromentArea.createBox(7);
    }

    private void SpawnAgent()
    {
        if (spawnField.Length > 0)
        {
            var spawn = spawnField[Random.Range(0, spawnField.Length)].transform;
            var xLocalPosition = spawn.localPosition.x;
            var zLocalPosition = spawn.localPosition.z;
            var xRange = spawn.localScale.x / 2;
            var zRange = spawn.localScale.z / 2;
            transform.localPosition = new Vector3(Random.Range(xLocalPosition - xRange, xLocalPosition + xRange), 1f,
                Random.Range(zLocalPosition - zRange, zLocalPosition + zRange));
            transform.rotation = Quaternion.Euler(new Vector3(0f, Random.Range(0, 360)));
        }
        else
        {
            Debug.Log("No spawn points found.");
        }
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        var discreteActionsOut = actionsOut.DiscreteActions;
        if (Random.value < epsilon)
        {
            discreteActionsOut[0] = generateRandomValue();
        }

        else
        {
            if (Input.GetKey(KeyCode.D))
            {
                discreteActionsOut[0] = 2;
            }
            else if (Input.GetKey(KeyCode.W))
            {
                discreteActionsOut[0] = 1;
            }
            else if (Input.GetKey(KeyCode.A))
            {
                discreteActionsOut[0] = 3;
            }
            else if (Input.GetKey(KeyCode.S))
            {
                discreteActionsOut[0] = 4;
            }
        }
    }

    private int generateRandomValue()
    {
        float random = Random.Range(0f, 1f);

        if (random < 0.4f)
        {
            return 1;
        }
        else if (random < 0.6f)
        {
            return 2;
        }
        else if (random < 0.8f)
        {
            return 3;
        }
        else
        {
            return 4;
        }
    }


    private void Update()
    {
        timeElapsed += Time.deltaTime;
        if (timeElapsed >= rewardInterval)
        {
            timeElapsed = 0f;
            float cumulativeReward = GetCumulativeReward();
            rewardText.text = cumulativeReward.ToString("0.00");
        }
    }

    Vector3 RoundVector3(Vector3 vector)
    {
        return new Vector3(
            (float)Math.Round(vector.x, 1),
            (float)Math.Round(vector.y, 1),
            (float)Math.Round(vector.z, 1)
        );
    }

    public void CreateTail()
    {
        var newObject = Instantiate(tail, Vector3.zero,
            Quaternion.Euler(0f, 0f, 0f), EnviromentArea.transform);
        PlaceObject(newObject);
    }

    private void PlaceObject(GameObject newObject)
    {
        newObject.transform.localPosition = transform.localPosition;
    }

    private void EndEpisodeCustom()
    {
        JsonWrite();
        EndEpisode();
    }

    private void JsonWrite()
    {
        string path = Application.dataPath + "/TrainingData.json";
        Debug.Log("JSON");

        Information information = new Information();
        information.reward = GetCumulativeReward();
        information.episode = this.episode;
        information.takeReward = this.takeReward;
        information.action = StepCount;
        AllInformations.Add(information);

        InformationList data = new InformationList();
        data.AllInformations = AllInformations;

        string jsonData = JsonUtility.ToJson(data, true);
        File.WriteAllText(path, jsonData);
    }
}
