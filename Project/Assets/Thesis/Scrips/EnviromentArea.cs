using Unity.MLAgentsExamples;
using UnityEngine;

public class EnviromentArea : Area
{
    public GameObject ball;
    public GameObject[] spawnBallArea;
    public GameObject boxObstacle;
    public GameObject floor;

    public void placeBall()
    {
        if (spawnBallArea.Length > 0)
        {
            var newBall = Instantiate(ball, Vector3.zero, Quaternion.Euler(0f, 0f, 0f), transform);
            newBall.transform.localPosition =
                getSpawnLocation(spawnBallArea[Random.Range(0, spawnBallArea.Length)].transform, 5f);
        }
        else
        {
            Debug.Log("No spawn points found.");
        }
    }

    private Vector3 getSpawnLocation(Transform spawn, float yLocation)
    {
        var xLocalPosition = spawn.localPosition.x;
        var zLocalPosition = spawn.localPosition.z;
        var xRange = spawn.localScale.x / 2.1f;
        var zRange = spawn.localScale.z / 2.1f;
        return new Vector3(Random.Range(xLocalPosition - xRange, xLocalPosition + xRange),
            yLocation, Random.Range(zLocalPosition - zRange, zLocalPosition + zRange));
    }

    public void createBox(int numberOfBox)
    {
        for (var i = 0; i < numberOfBox; i++)
        {
            var newObject = Instantiate(boxObstacle, Vector3.zero,
                Quaternion.Euler(0f, 0f, 0f), transform);
            PlaceObject(newObject);
        }
    }

    private void PlaceObject(GameObject newObject)
    {
        newObject.transform.localPosition = getSpawnLocation(floor.transform, 10f);
    }

    public void ClearEnviroment()
    {
        foreach (Transform child in transform)
        {
            if (child.CompareTag("box") || child.CompareTag("goal") || child.CompareTag("tail"))
            {
                Destroy(child.gameObject);
            }
        }
    }
}
