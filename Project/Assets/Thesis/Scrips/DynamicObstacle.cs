using UnityEngine;

public class DynamicObstacle : MonoBehaviour
{
    public GameObject[] waypoints;
    private int current = 0;
    private readonly float speed = 10;
    private readonly float WPradius = 1;

    void Update()
    {
        if (Vector3.Distance(waypoints[current].transform.localPosition, transform.localPosition) < WPradius)
        {
            current++;
            if (current >= waypoints.Length)
            {
                current = 0;
            }
        }

        transform.localPosition = Vector3.MoveTowards(transform.localPosition,
            waypoints[current].transform.localPosition, Time.deltaTime * speed * 0.3f);
    }
}
