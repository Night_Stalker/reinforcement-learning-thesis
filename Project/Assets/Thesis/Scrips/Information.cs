using System;

[Serializable]
public class Information
{
    public int episode;
    public float reward;
    public bool takeReward;
    public int action;
}
