# Саад тойрох үйлдэлд рэинфорсмэнт сургалтыг ашиглах судалгаа

## Дипломын тайлан

Дипломын тайланг [энд](Thesis%20Paper/Дипломын%20тайлан.pdf) дарж үзнэ үү.

## Юнити тайлбар

Энэхүү судалгааны ажлыг 'UNITY' платформын 2022.3.4f1 эдитор дээр хийсэн ба сургалтын орчин нь
['reanforcement-learning.unity'](reanforcement-learning.unity) файлаар энэхүү хэсэгт байгаа.

## Виртуал орчин

Виртуал орчин хэрхэн үүсгэх зааврыг [энд](https://github.com/Unity-Technologies/ml-agents/tree/develop/ml-agents) дарж харна уу.

## Командууд

Сургалтын командыг ашиглахын тулд 'Anaconda' эсвэл 'miniconda' татсан байх шаардлагатай.
Үүний дараа 'Anaconda prompt' эсвэл 'miniconda prompt' нээж файлын замыг зааж өгнө. 
Жишээ нь:
```
cd  C:\Users\telmen\Documents\RL\ml-agents
```
Үүний дараа виртуал орчинг идвэхжүүлнэ. Өөрийн виртуал орчингийн нэрийг 'ml-agents'-ийн оронд бичнэ.
```
conda activate ml-agents
```
Виртуал орчинг идвэхжүүлсний дараагаар юнити дээр сургах орчныг гаргасан байна. Анх сургах гэж байгаа бол агентын
'Behavior Parameters'-ээс доорх 2-ыг тааруулах хэрэгтэй.
- Model -> None
- Behavior Type -> Default

![](Images/Modal.png)

Үүний дараагаар доорх зам дээр байх 'movetotarget.yaml' байх алгоритмын параметртэйгээр сургалтыг эхлүүлэх ба
үр дүнг 'Name' нэрээр 'result' хэсэгт байрлуулна.
```
mlagents-learn config/ppo/movetotarget.yaml --run-id=Name
```

Үр дүнг 'tensorboard ' ашиглан харах бол доорх командыг хэрэгжүүлж веб хөтчөөс http://localhost:6006/ 
линкээр орж харна.
```
tensorboard --logdir=results
```

## Дэлгэрэнгүй мэдээллийг [энд](https://github.com/Unity-Technologies/ml-agents) дарж харна уу.
